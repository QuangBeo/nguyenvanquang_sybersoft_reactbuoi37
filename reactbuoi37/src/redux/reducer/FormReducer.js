import { nanoid } from "nanoid";

let initialState = {
  userList: [
    {
      MaSV: "1",
      HoTen: "Nguyễn Văn Quang",
      SoDt: "1234567890",
      Email: "nguyenvanquang100399xxx@gmail.com",
    },
    {
      MaSV: "2",
      HoTen: "Nguyễn Văn Chiến",
      SoDt: "987654321",
      Email: "nguyenvanchienNobi2001@gmail.com",
    },
    {
      MaSV: "3",
      HoTen: "Nguyễn Văn Vinh",
      SoDt: "5678904321",
      Email: "quangvinhchaucanh123@gmail.com",
    },
  ],
  userEdit: null,
};

export const FormReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "THEM_SINH_VIEN": {
      let cloneUserList = [...state.userList, payload];
      return {
        ...state,
        userList: cloneUserList,
      };
    }
    case "XOA_SINH_VIEN": {
      let cloneUserList = [...state.userList];
      let index = cloneUserList.findIndex((index) => {
        return index.MaSV == payload.MaSV;
      });
      if (index != -1) {
        cloneUserList.splice(index, 1);
      }
      return {
        ...state,
        userList: cloneUserList,
      };
    }
    case "LAY_THONG_TIN": {
      let cloneUserEdit = payload;
      document.getElementById("maSV").disabled = true;
      return {
        ...state,
        userEdit: cloneUserEdit,
      };
    }
    case "SUA_SINH_VIEN": {
      let cloneUserUpdate = [...state.userList];
      let index = cloneUserUpdate.findIndex((index) => {
        return index.MaSV == payload.MaSV;
      });
      console.log("index: ", index);
      if (index != -1) {
        cloneUserUpdate[index] = payload;
      }
      return {
        ...state,
        userList: cloneUserUpdate,
      };
    }
    case "SEARCH_SINH_VIEN": {
      let cloneUserSearch = [...state.userList];
      console.log("cloneUserSearch: ", cloneUserSearch);
      let index = cloneUserSearch.findIndex((index) => {
        return index.MaSV == payload;
      });

      if(index != -1) {
       cloneUserSearch = [cloneUserSearch[index]];
      }
      else{
        alert("Không tồn tại sinh viên");
      }
      return {
        ...state,
        userList: cloneUserSearch,
      };
    }
    default:
      return state;
  }
};
