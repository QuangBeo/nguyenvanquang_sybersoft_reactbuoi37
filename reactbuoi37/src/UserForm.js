import React, { Component } from "react";
import { connect } from "react-redux";
import { FormReducer } from "./redux/reducer/FormReducer";

class UserForm extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }
  state = {
    user: {
      MaSV: "",
      HoTen: "",
      SoDt: "",
      Email: "",
      search: null,
    },
  };
  handleGetUserForm = (event) => {
    //name: này là name của input
    //key: đổi tên name thành
    let { value, name: key } = event.target;
    let cloneUser = { ...this.state.user };
    // name này là name của object

    // let user = {
    //     name: "alice",
    //     age: 18,
    // }
    // user.name = "bob";
    // let key = "name";
    // user[key] = "bob";

    cloneUser[key] = value;
    this.setState({ user: cloneUser }, () => {
      console.log(this.state.user);
    });
  };
  componentDidMount() {
    // this.inputRef.current.value = "Alice";
    // this.inputRef.current.style.color = "red";
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.userEdit) {
      this.setState({
        user: nextProps.userEdit,
      });
    }
  }
  render() {
    return (
      <div className="row py-5">
        <div className="col-12 text-center bg-dark text-light">
          <h1>THÔNG TIN SINH VIÊN</h1>
        </div>
        <div className="form-group col-6">
          <label className="font-weight-bold">Mã Sinh Viên</label>
          <input
            ref={this.inputRef}
            value={this.state.user.MaSV}
            onChange={this.handleGetUserForm}
            type="text"
            id="maSV"
            className="form-control"
            //"maSV" phải trùng với key trong object(state)
            name="MaSV"
            placeholder="Mã Sinh Viên"
          />
          <label className="font-weight-bold">Họ Và Tên</label>
          <input
            value={this.state.user.HoTen}
            onChange={this.handleGetUserForm}
            type="text"
            className="form-control"
            name="HoTen"
            placeholder="Họ và Tên"
          />
        </div>
        <div className="form-group col-6">
          <label className="font-weight-bold">Số Điện Thoại</label>
          <input
            value={this.state.user.SoDt}
            onChange={this.handleGetUserForm}
            type="text"
            className="form-control"
            name="SoDt"
            placeholder="Số Điện Thoại"
          />
          <label className="font-weight-bold">Email</label>
          <input
            value={this.state.user.Email}
            onChange={this.handleGetUserForm}
            type="text"
            className="form-control"
            name="Email"
            placeholder="Email"
          />
        </div>
        <div className="col-12">
          <button
            className="btn btn-success"
            onClick={() => {
              this.props.handleThemSV(this.state.user);
            }}
          >
            Thêm Sinh Viên
          </button>
          <button
            className="btn btn-warning ml-3"
            onClick={() => {
              this.props.hanldSuaSV(this.state.user);
            }}
          >
            Sửa Sinh Viên
          </button>
        </div>
        <div className="col-12">
          <div className="form-group d-flex justify-content-center mt-3">
            <input
              value={this.state.search}
              onChange={this.handleGetUserForm}
              name="search"
              type="text"
              className="form-control w-25"
              placeholder="Nhập Mã Sinh viên"
            />
            <span onClick={() => {this.props.handleSearchSV(this.state.user.search)}} className="btn btn-secondary">
              <i class="fa fa-search"></i>
            </span>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    userEdit: state.FormReducer.userEdit,
  };
};

const mapDisPatchToProps = (dispatch) => {
  return {
    handleThemSV: (value) => {
      dispatch({
        type: "THEM_SINH_VIEN",
        payload: value,
      });
    },
    hanldSuaSV: (value) => {
      dispatch({
        type: "SUA_SINH_VIEN",
        payload: value,
      });
    },
    handleSearchSV: (value) => {
      dispatch({
        type: "SEARCH_SINH_VIEN",
        payload: value,
      })
    }
  };
};

export default connect(mapStateToProps, mapDisPatchToProps)(UserForm);
