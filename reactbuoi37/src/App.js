import logo from './logo.svg';
import './App.css';
import ReactFormReducer from './ReactFormReducer';

function App() {
  return (
    <div className="App">
      <ReactFormReducer/>
    </div>
  );
}

export default App;
