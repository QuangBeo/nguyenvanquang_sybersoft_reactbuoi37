import React, { Component } from 'react'
import TableForm from './TableForm'
import UserForm from './UserForm'

export default class ReactFormReducer extends Component {
  render() {
    return (
      <div className='container mx-auto'>
        <UserForm/>
        <TableForm/>
      </div>
    )
  }
}
