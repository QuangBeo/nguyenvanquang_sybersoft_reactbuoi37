import React, { Component } from "react";
import { connect } from "react-redux";
import { FormReducer } from "./redux/reducer/FormReducer";

class TableForm extends Component {
  RenderDanhSachSinhVien = () => {
    return this.props.userList.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.MaSV}</td>
          <td>{item.HoTen}</td>
          <td>{item.SoDt}</td>
          <td>{item.Email}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemoveSV(item);
              }}
              className="btn btn-danger mr-2"
            >
              Xoá
            </button>
            <button
            onClick={() => {this.props.hanldChangeSV(item)}}
            className="btn btn-secondary">Sửa</button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr className="bg-dark text-light">
            <th>Mã Sinh Viên</th>
            <th>Họ Và Tên</th>
            <th>Số Điện Thoại</th>
            <th>Email</th>
            <th></th>
          </tr>
        </thead>
        <tbody>{this.RenderDanhSachSinhVien()}</tbody>
      </table>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userList: state.FormReducer.userList,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleRemoveSV: (value) => {
      dispatch({
        type: "XOA_SINH_VIEN",
        payload: value,
      });
    },
    hanldChangeSV: (value) => {
        dispatch({
            type: "LAY_THONG_TIN",
            payload: value,
        })
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TableForm);
